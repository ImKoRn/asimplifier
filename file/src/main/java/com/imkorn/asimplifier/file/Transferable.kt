package com.imkorn.asimplifier.file

import android.content.ContentResolver
import android.net.Uri
import android.os.ParcelFileDescriptor
import java.io.*

interface Transferable {

    companion object {
        fun File.toTransferable(): Transferable = TransferableFile(this)
        fun FileDescriptor.toTransferable(): Transferable = TransferableFD(this)
        fun ParcelFileDescriptor.toTransferable(): Transferable = TransferableFD(fileDescriptor)
        fun Uri.toTransferable(): Transferable = when(scheme) {
            ContentResolver.SCHEME_FILE     -> TransferableFile(File(path))
            else                            -> throw IllegalArgumentException("Unable to create Transferable from $this")
        }
    }

    fun openInputStream(): FileInputStream
    fun openOutputStream(): FileOutputStream
}

class TransferableFD(val fd: FileDescriptor) : Transferable {
    override fun openInputStream(): FileInputStream = FileInputStream(fd)
    override fun openOutputStream(): FileOutputStream = FileOutputStream(fd)
}

class TransferableFile(val file: File) : Transferable {
    override fun openInputStream(): FileInputStream = file.inputStream()
    override fun openOutputStream(): FileOutputStream = file.outputStream()
}