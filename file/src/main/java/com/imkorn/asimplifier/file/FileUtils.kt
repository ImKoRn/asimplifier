package com.imkorn.asimplifier.file

import java.io.*
import java.nio.channels.FileChannel

object FileUtils {

    @Throws(FileSystemException::class)
    fun File.deleteRecursive(include: Boolean = true, filter: ((file: File) -> Boolean)? = null): Boolean {
        if(!exists()) return false
        deleteRecursiveImpl(include, filter)
        return true
    }

    @Throws(FileSystemException::class)
    fun Array<File>.deleteRecursive(include: Boolean = true,
                                    filter: ((file: File) -> Boolean)? = null,
                                    progress: ((file: File, index: Int, count: Int) -> Unit)? = null): Boolean {
        if(isEmpty()) return false
        deleteRecursiveImpl(include, filter, progress)
        return true
    }

    @Throws(FileSystemException::class)
    fun Collection<File>.deleteRecursive(include: Boolean = true,
                                         filter: ((file: File) -> Boolean)? = null,
                                         progress: ((file: File, index: Int, count: Int) -> Unit)? = null): Boolean {
        if(isEmpty()) return false
        deleteRecursiveImpl(include, filter, progress)
        return true
    }

    @Throws(FileSystemException::class)
    fun File.copyFile(destinationFile: File,
                      progress: ((consumed: Long, size: Long) -> Unit)? = null): Boolean {
        if(!exists() || !isFile) return false
        copyFileImpl(destinationFile, progress)
        return true
    }

    @Throws(FileSystemException::class)
    fun File.copyDir(destinationDir: File,
                     recursive: Boolean = true,
                     progress: ((file: File, consumed: Long, size: Long) -> Unit)? = null): Boolean {
        if(!exists() || !isDirectory) return false
        copyDirImpl(destinationDir, recursive, progress)
        return true
    }

    @Throws(IOException::class)
    fun FileInputStream.transferAll(destination: FileOutputStream,
                                    progress: ((consumed: Long, size: Long) -> Unit)? = null) {
        val input = channel
        val output = destination.channel
        try {
            input.transferAll(output, progress)
        } finally {
            input.close()
            output.close()
        }
    }

    @Throws(IOException::class)
    fun FileChannel.transferAll(destination: FileChannel,
                                progress: ((consumed: Long, size: Long) -> Unit)? = null) {
        val size: Long = size()
        var position: Long = 0
        while(true) {
            progress?.invoke(position, size)
            if(position >= size) return
            position += transferTo(position, size, destination)
        }
    }

    @Throws(FileSystemException::class)
    private fun File.deleteRecursiveImpl(include: Boolean = true, filter: ((file: File) -> Boolean)? = null) {
        listFiles()?.forEach { file -> file.deleteRecursiveImpl(true, filter) }
        if(include && filter?.invoke(this) != false) {
            try {
                if(exists() && !delete()) throw FileSystemException(this, reason = "Can't delete file or directory")
            } catch (e: FileSystemException) {
                throw e
            } catch (e: Throwable) {
                throw FileSystemException(this, reason = "Can't delete file or directory").initCause(e)
            }
        }
    }

    @Throws(FileSystemException::class)
    private fun Array<File>.deleteRecursiveImpl(include: Boolean = true,
                                                filter: ((file: File) -> Boolean)? = null,
                                                progress: ((file: File, index: Int, count: Int) -> Unit)? = null) {
        forEachIndexed { index, file ->
            progress?.invoke(file, index, size)
            if(file.exists()) file.deleteRecursiveImpl(include, filter)
        }
    }

    @Throws(FileSystemException::class)
    private fun Collection<File>.deleteRecursiveImpl(include: Boolean = true,
                                                     filter: ((file: File) -> Boolean)? = null,
                                                     progress: ((file: File, index: Int, count: Int) -> Unit)? = null) {
        forEachIndexed { index, file ->
            progress?.invoke(file, index, size)
            if(file.exists()) file.deleteRecursiveImpl(include, filter)
        }
    }

    @Throws(FileSystemException::class)
    private fun File.copyFileImpl(destination: File,
                                  progress: ((consumed: Long, size: Long) -> Unit)? = null) {
        val input = FileInputStream(this)
        val output = FileOutputStream(destination)

        try {
            input.transferAll(output, progress)
        } catch (e: Throwable) {
            throw FileSystemException(this, destination, "File copy failed").initCause(e)
        } finally {
            input.close()
            output.close()
        }
    }

    @Throws(FileSystemException::class)
    private fun File.copyDirImpl(destinationDir: File,
                                 recursive: Boolean = true,
                                 progress: ((file: File, consumed: Long, size: Long) -> Unit)? = null) {
        val dir = File(destinationDir, name)
        try {
            if(!dir.exists() && !dir.mkdirs()) throw FileSystemException(this, dir, reason = "Can't create directory")
            progress?.invoke(dir, 0L, 0L)
            listFiles()?.forEach { file ->
                if(file.isDirectory) {
                    if(recursive) file.copyDirImpl(File(dir, file.name), true, progress)
                } else if(file.isFile) {
                    if(progress != null) {
                        file.copyFileImpl(File(dir, file.name)) { consumed, size -> progress(file, consumed, size) }
                    } else {
                        file.copyFileImpl(File(dir, file.name))
                    }
                }
            }
        } catch (e: FileSystemException) {
            throw e
        } catch (e: Throwable) {
            throw FileSystemException(this, dir, reason = "Can't copy file or directory").initCause(e)
        }
    }
}