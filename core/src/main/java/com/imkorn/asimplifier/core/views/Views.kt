package com.imkorn.asimplifier.core.views

import android.content.res.TypedArray
import android.util.AttributeSet
import androidx.transition.Transition
import android.view.View
import android.view.ViewGroup
import androidx.annotation.AttrRes
import androidx.annotation.StyleRes
import androidx.annotation.StyleableRes
import androidx.transition.TransitionManager

object Views {
    @JvmStatic
    fun View.hide(full: Boolean = true) {
        visibility = if (full) View.GONE else View.INVISIBLE
    }

    @JvmStatic
    fun View.show() {
        visibility = View.VISIBLE
    }

    @JvmStatic
    inline fun ViewGroup.animate(transition: Transition? = null, action: () -> Unit) {
        TransitionManager.beginDelayedTransition(this, transition)
        action()
    }

    @JvmStatic
    inline fun View.withAttributes(set: AttributeSet? = null,
                                      @StyleableRes attrs: IntArray,
                                      @AttrRes defStyleAttr: Int = 0,
                                      @StyleRes defStyleRes: Int = 0,
                                      action: TypedArray.() -> Unit) {
        with(context.obtainStyledAttributes(set, attrs, defStyleAttr, defStyleRes)) {
            try {
                action()
            }
            finally {
                recycle()
            }
        }
    }
}