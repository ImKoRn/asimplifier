package com.imkorn.asimplifier.core.collections

import android.util.SparseArray
import android.util.SparseBooleanArray
import android.util.SparseIntArray
import android.util.SparseLongArray

object Collections {

    // Array

    @JvmStatic
    fun <Type> Array<Type>?.isNonNullAndEmpty() = this != null && size != 0

    @JvmStatic
    inline fun <Type> Array<Type>?.whenNonNullAndEmpty(action: Array<Type>.() -> Unit) {
        if(this != null && size != 0) action()
    }

    @JvmStatic
    fun <Type> Array<Type>?.isNullOrEmpty(): Boolean = this == null || size == 0

    @JvmStatic
    inline fun <Type> Array<Type>?.whenNullOrEmpty(action: () -> Unit) {
        if(this == null || size == 0) action()
    }

    @JvmStatic
    inline fun <Type> Array<Type>?.orElse(action: () -> Array<Type>) = if(this == null || size == 0) action() else this

    @JvmStatic
    inline fun <Type> Array<Type>.getValueOrElse(position: Int, action: Array<Type>.() -> Type): Type = get(position) ?: action()

    // Collection

    @JvmStatic
    fun <Type> Collection<Type>?.isNonNullAndEmpty() = this != null && size != 0

    @JvmStatic
    inline fun <Type> Collection<Type>?.whenNonNullAndEmpty(action: Collection<Type>.() -> Unit) {
        if(this != null && size != 0) action()
    }

    @JvmStatic
    fun <Type> Collection<Type>?.isNullOrEmpty(): Boolean = this == null || size == 0

    @JvmStatic
    inline fun <Type> Collection<Type>?.whenNullOrEmpty(action: () -> Unit) {
        if (this == null || size == 0) action()
    }

    @JvmStatic
    inline fun <Type> Collection<Type>?.orElse(action: () -> Collection<Type>) = if(this == null || size == 0) action() else this

    // List

    @JvmStatic
    inline fun <Type> List<Type>.getValueOrElse(position: Int, action: List<Type>.() -> Type): Type = get(position) ?: action()

    // SparseArray

    @JvmStatic
    fun <Type> SparseArray<Type>.isNotEmpty() = !isEmpty()

    @JvmStatic
    fun <Type> SparseArray<Type>.isEmpty() = size() == 0

    @JvmStatic
    fun <Type> SparseArray<Type>?.isNonNullAndEmpty() = this != null && size() != 0

    @JvmStatic
    inline fun <Type> SparseArray<Type>?.whenNonNullAndEmpty(action: SparseArray<Type>.() -> Unit) {
        if(this != null && size() != 0) action()
    }

    @JvmStatic
    fun <Type> SparseArray<Type>?.isNullOrEmpty(): Boolean = this == null || size() == 0

    @JvmStatic
    inline fun <Type> SparseArray<Type>?.whenNullOrEmpty(action: () -> Unit) {
        if(this == null || size() == 0) action()
    }

    @JvmStatic
    inline fun <Type> SparseArray<Type>?.orElse(action: () -> Array<Type>) = if(this == null || size() == 0) action() else this

    @JvmStatic
    inline fun <Type> SparseArray<Type>.getValueOrElse(position: Int, action: SparseArray<Type>.() -> Type): Type = get(position) ?: action()

    @JvmStatic
    fun <Type> SparseArray<Type>.set(key: Int, value: Type): Type {
        put(key, value)
        return value
    }

    @JvmStatic
    inline fun <Type> SparseArray<Type>.forEach(action: (value: Type) -> Unit) {
        for (position in 0 until size()) action(valueAt(position))
    }

    @JvmStatic
    inline fun <Type> SparseArray<Type>.forEachFull(action: (key: Int, value: Type) -> Unit) {
        for (position in 0 until size()) action(keyAt(position), valueAt(position))
    }

    @JvmStatic
    inline fun <Type> SparseArray<Type>.forEachIndexed(action: (position: Int, value: Type) -> Unit) {
        for (position in 0 until size()) action(position, valueAt(position))
    }

    @JvmStatic
    inline fun <Type> SparseArray<Type>.forEachFullIndexed(action: (position: Int, key: Int, value: Type) -> Unit) {
        for (position in 0 until size()) action(position, keyAt(position), valueAt(position))
    }

    // SparseBooleanArray

    @JvmStatic
    fun SparseBooleanArray.isNotEmpty() = !isEmpty()

    @JvmStatic
    fun SparseBooleanArray.isEmpty() = size() == 0

    @JvmStatic
    fun SparseBooleanArray?.isNonNullAndEmpty() = this != null && size() != 0

    @JvmStatic
    inline fun SparseBooleanArray?.whenNonNullAndEmpty(action: SparseBooleanArray.() -> Unit) {
        if(this != null && size() != 0) action()
    }

    @JvmStatic
    fun SparseBooleanArray?.isNullOrEmpty(): Boolean = this == null || size() == 0

    @JvmStatic
    inline fun SparseBooleanArray?.whenNullOrEmpty(action: () -> Unit) {
        if(this == null || size() == 0) action()
    }

    @JvmStatic
    inline fun SparseBooleanArray?.orElse(action: () -> SparseBooleanArray) = if(this == null || size() == 0) action() else this

    @JvmStatic
    fun SparseBooleanArray.set(key: Int, value: Boolean): Boolean {
        put(key, value)
        return value
    }

    @JvmStatic
    inline fun SparseBooleanArray.forEach(action: (value: Boolean) -> Unit) {
        for (position in 0 until size()) action(valueAt(position))
    }

    @JvmStatic
    inline fun SparseBooleanArray.forEachFull(action: (key: Int, value: Boolean) -> Unit) {
        for (position in 0 until size()) action(keyAt(position), valueAt(position))
    }

    @JvmStatic
    inline fun SparseBooleanArray.forEachIndexed(action: (position: Int, value: Boolean) -> Unit) {
        for (position in 0 until size()) action(position, valueAt(position))
    }

    @JvmStatic
    inline fun SparseBooleanArray.forEachFullIndexed(action: (position: Int, key: Int, value: Boolean) -> Unit) {
        for (position in 0 until size()) action(position, keyAt(position), valueAt(position))
    }

    // SparseIntArray

    @JvmStatic
    fun SparseIntArray.isNotEmpty() = !isEmpty()

    @JvmStatic
    fun SparseIntArray.isEmpty() = size() == 0

    @JvmStatic
    fun SparseIntArray?.isNonNullAndEmpty() = this != null && size() != 0

    @JvmStatic
    inline fun SparseIntArray?.whenNonNullAndEmpty(action: SparseIntArray.() -> Unit) {
        if(this != null && size() != 0) action()
    }

    @JvmStatic
    fun SparseIntArray?.isNullOrEmpty(): Boolean = this == null || size() == 0

    @JvmStatic
    inline fun SparseIntArray?.whenNullOrEmpty(action: () -> Unit) {
        if(this == null || size() == 0) action()
    }

    @JvmStatic
    inline fun SparseIntArray?.orElse(action: () -> SparseIntArray) = if(this == null || size() == 0) action() else this

    @JvmStatic
    fun SparseIntArray.set(key: Int, value: Int): Int {
        put(key, value)
        return value
    }

    @JvmStatic
    inline fun SparseIntArray.forEach(action: (value: Int) -> Unit) {
        for (position in 0 until size()) action(valueAt(position))
    }

    @JvmStatic
    inline fun SparseIntArray.forEachFull(action: (key: Int, value: Int) -> Unit) {
        for (position in 0 until size()) action(keyAt(position), valueAt(position))
    }

    @JvmStatic
    inline fun SparseIntArray.forEachIndexed(action: (position: Int, value: Int) -> Unit) {
        for (position in 0 until size()) action(position, valueAt(position))
    }

    @JvmStatic
    inline fun SparseIntArray.forEachFullIndexed(action: (position: Int, key: Int, value: Int) -> Unit) {
        for (position in 0 until size()) action(position, keyAt(position), valueAt(position))
    }

    // SparseLongArray

    @JvmStatic
    fun SparseLongArray.isNotEmpty() = !isEmpty()

    @JvmStatic
    fun SparseLongArray.isEmpty() = size() == 0

    @JvmStatic
    fun SparseLongArray?.isNonNullAndEmpty() = this != null && size() != 0

    @JvmStatic
    inline fun SparseLongArray?.whenNonNullAndEmpty(action: SparseLongArray.() -> Unit) {
        if(this != null && size() != 0) action()
    }

    @JvmStatic
    fun SparseLongArray?.isNullOrEmpty(): Boolean = this == null || size() == 0

    @JvmStatic
    inline fun SparseLongArray?.whenNullOrEmpty(action: () -> Unit) {
        if(this == null || size() == 0) action()
    }

    @JvmStatic
    inline fun SparseLongArray?.orElse(action: () -> SparseLongArray) = if(this == null || size() == 0) action() else this


    @JvmStatic
    fun SparseLongArray.set(key: Int, value: Long): Long {
        put(key, value)
        return value
    }

    @JvmStatic
    inline fun SparseLongArray.forEach(action: (value: Long) -> Unit) {
        for (position in 0 until size()) action(valueAt(position))
    }

    @JvmStatic
    inline fun SparseLongArray.forEachFull(action: (key: Int, value: Long) -> Unit) {
        for (position in 0 until size()) action(keyAt(position), valueAt(position))
    }

    @JvmStatic
    inline fun SparseLongArray.forEachIndexed(action: (position: Int, value: Long) -> Unit) {
        for (position in 0 until size()) action(position, valueAt(position))
    }

    @JvmStatic
    inline fun SparseLongArray.forEachFullIndexed(action: (position: Int, key: Int, value: Long) -> Unit) {
        for (position in 0 until size()) action(position, keyAt(position), valueAt(position))
    }
}