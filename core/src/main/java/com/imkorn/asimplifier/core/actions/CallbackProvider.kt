package com.imkorn.asimplifier.core.actions

import kotlin.reflect.KClass

interface CallbackProvider {
    fun getCallback(clazz: KClass<out Callback>): Callback = throw NotImplementedError("$clazz not found")
}