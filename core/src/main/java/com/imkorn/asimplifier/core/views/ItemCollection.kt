package com.imkorn.asimplifier.core.views

import android.util.SparseArray

interface ItemCollection<ItemType> {

    fun getCount(): Int

    operator fun get(position: Int): ItemType
    operator fun set(position: Int, item: ItemType): ItemType

    fun addTo(position: Int, item: ItemType)
    fun addFirst(item: ItemType) = addTo(0, item)
    fun addLast(item: ItemType) = addTo(getCount(), item)

    fun addAll(items: Array<ItemType>)
    fun addAll(items: Collection<ItemType>)
    fun addAll(items: SparseArray<ItemType>)

    fun setAll(items: Array<ItemType>)
    fun setAll(items: Collection<ItemType>)
    fun setAll(items: SparseArray<ItemType>)

    fun find(item: ItemType): Int = INVALID_POSITION

    fun forEach(action: (ItemType) -> Unit) = forEach { _, item -> action(item)}
    fun forEach(action: (Int, ItemType) -> Unit)

    fun forEachUntil(predicate: (ItemType) -> Boolean): Int = forEachUntil { _, item -> predicate(item) }
    fun forEachUntil(predicate: (Int, ItemType) -> Boolean): Int = INVALID_POSITION

    fun forEachRemove(predicate: (item: ItemType) -> Boolean) =
        forEachRemove { _, item -> predicate(item) }
    fun forEachRemove(predicate: (position: Int, item: ItemType) -> Boolean) =
        forEachRemove(predicate, { Unit }, { _, _, _ -> Unit })
    fun <CollectionType> forEachRemove(
            predicate: (position: Int, item: ItemType) -> Boolean,
            initializer: () -> CollectionType,
            action: (collection: CollectionType, position: Int, item: ItemType) -> Unit
    ): CollectionType

    fun remove(item: ItemType): ItemType?
    fun removeAt(position: Int): ItemType
    fun clear()

    fun update(position: Int)
    fun update(start: Int, count: Int)
    fun update(item: ItemType)
    fun update(predicate: (ItemType) -> Boolean) = update { _, item -> predicate(item) }
    fun update(predicate: (Int, ItemType) -> Boolean)
    fun updateAll()

    companion object {
        const val INVALID_POSITION = -1
    }
}
