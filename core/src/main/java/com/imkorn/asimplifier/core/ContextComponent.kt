package com.imkorn.asimplifier.core

import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.IBinder
import android.os.ResultReceiver
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

interface ContextComponent {

    private val resources get() = getContext().resources

    fun getContext(): Context

    // Resources
    fun Int.asLayout(container: ViewGroup? = null, attach: Boolean = false): View =
            LayoutInflater.from(getContext()).inflate(this, container, attach)
    fun Int.asDrawable(): Drawable = ContextCompat.getDrawable(getContext(), this)!!
    fun Int.asColor(): Int = ContextCompat.getColor(getContext(), this)
    fun Int.asColorStateList(): ColorStateList = ContextCompat.getColorStateList(getContext(), this)!!
    fun Int.asString(): String = resources.getString(this)
    fun Int.asString(vararg args: String): String = resources.getString(this, args)
    fun Int.asDimen(): Float = resources.getDimension(this)
    fun Int.asDimenPO(): Int = resources.getDimensionPixelOffset(this)
    fun Int.asDimenPS(): Int = resources.getDimensionPixelSize(this)
    fun Int.asBool(): Boolean = resources.getBoolean(this)
    fun Int.asQuantity(quantity: Int): String = resources.getQuantityString(this, quantity)
    fun Int.asQuantity(quantity: Int, vararg args: String): String = resources.getQuantityString(this, quantity, args)
    // Utils
    fun toast(msg: String, duration: Int = Toast.LENGTH_SHORT) = Toast.makeText(getContext(), msg, duration).show()
    fun toast(@StringRes msgRes: Int, duration: Int = Toast.LENGTH_SHORT) = toast(msgRes.asString(), duration)
    // Permissions
    @TargetApi(Build.VERSION_CODES.M)
    fun Collection<String>.isGranted(): Boolean = all { permission -> permission.isGranted() }
    fun Array<String>.isGranted(): Boolean = all { permission -> permission.isGranted() }
    @TargetApi(Build.VERSION_CODES.M)
    fun String.isGranted(): Boolean = getContext().checkSelfPermission(this) == PackageManager.PERMISSION_GRANTED

    companion object {
        // Versions
        fun preL(): Boolean = Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP
        fun preM(): Boolean = Build.VERSION.SDK_INT < Build.VERSION_CODES.M
        fun preO(): Boolean = Build.VERSION.SDK_INT < Build.VERSION_CODES.O
        fun Int.pre(): Boolean = Build.VERSION.SDK_INT < this
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        fun sinceL(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
        @TargetApi(Build.VERSION_CODES.M)
        fun sinceM(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
        @TargetApi(Build.VERSION_CODES.O)
        fun sinceO(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
        fun Int.since(): Boolean = Build.VERSION.SDK_INT >= this
    }
}