package com.imkorn.asimplifier.core.views

import android.view.View
import com.imkorn.asimplifier.core.ContextComponent

interface ViewHolderBase<ItemType> : ContextComponent {
    fun bind(item: ItemType) = Unit
    fun <Type : View?> Int.find(): Type
}