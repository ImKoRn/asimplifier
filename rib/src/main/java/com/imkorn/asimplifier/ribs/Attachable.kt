package com.imkorn.asimplifier.ribs

import io.reactivex.disposables.Disposable

interface Attachable {
    fun attach(): Disposable
}