package com.imkorn.asimplifier.ribs

import android.os.Looper
import androidx.lifecycle.MutableLiveData

object LiveDatas {
    fun <T> MutableLiveData<T>.sync(value: T) {
        if (Thread.currentThread() == Looper.getMainLooper().thread) {
            this.value = value
        } else {
            this.postValue(value)
        }
    }
}
