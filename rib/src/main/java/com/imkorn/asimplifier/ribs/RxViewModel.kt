package com.imkorn.asimplifier.ribs

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.imkorn.asimplifier.ribs.LiveDatas.sync
import com.imkorn.asimplifier.rx2.Observables.subscribe
import com.imkorn.asimplifier.rx2.Maybes.subscribe
import com.imkorn.asimplifier.rx2.Flowables.subscribe
import com.imkorn.asimplifier.rx2.Singles.subscribe
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableContainer

open class RxViewModel : ViewModel(), DisposableContainer, Disposable {

    private val disposables = CompositeDisposable()

    fun addAll(vararg disposables: Disposable): Boolean = this.disposables.addAll(*disposables)
    override fun add(disposable: Disposable): Boolean = disposables.add(disposable)
    override fun remove(disposable: Disposable): Boolean = disposables.remove(disposable)
    override fun delete(disposable: Disposable): Boolean = disposables.delete(disposable)
    override fun dispose() = disposables.dispose()
    override fun isDisposed(): Boolean = disposables.isDisposed

    override fun onCleared() {
        super.onCleared()
        dispose()
    }

    inline fun <Type> Observable<Type>.subscribe(
            liveData: MutableLiveData<Type>,
            crossinline onComplete: () -> Unit = {},
            crossinline onError: (Throwable) -> Unit = { throw it }
    ) {
        subscribe(
                onSubscribe = { add(it) },
                onNext = { liveData.sync(it) },
                onComplete = {
                    delete(it)
                    onComplete()
                },
                onError = { t, d ->
                    delete(d)
                    onError(t)
                }
        )
    }

    inline fun <Type> Flowable<Type>.subscribe(
            liveData: MutableLiveData<Type>,
            crossinline onError: (Throwable) -> Unit = { throw it }
    ) {
        subscribe(
                onSubscribe = { add(it) },
                onNext = { liveData.sync(it) },
                onError = { t, d ->
                    delete(d)
                    onError(t)
                }
        )
    }

    inline fun <Type> Maybe<Type>.subscribe(
            liveData: MutableLiveData<Type>,
            crossinline onComplete: () -> Unit = {},
            crossinline onError: (Throwable) -> Unit = { throw it }
    ) {
        subscribe(
                onSubscribe = { add(it) },
                onSuccess = { v, d ->
                    delete(d)
                    liveData.sync(v)
                },
                onComplete = {
                    delete(it)
                    onComplete()
                },
                onError = { t, d ->
                    delete(d)
                    onError(t)
                }
        )
    }

    inline fun <Type> Single<Type>.subscribe(
            liveData: MutableLiveData<Type>,
            crossinline onError: (Throwable) -> Unit = { throw it }
    ) {
        subscribe(
                onSubscribe = { add(it) },
                onSuccess = { v, d ->
                    delete(d)
                    liveData.sync(v)
                },
                onError = { t, d ->
                    delete(d)
                    onError(t)
                }
        )
    }
}