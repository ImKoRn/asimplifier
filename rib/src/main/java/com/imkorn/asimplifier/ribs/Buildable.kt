package com.imkorn.asimplifier.ribs

interface Buildable<Type> {
    fun build(): Type
}