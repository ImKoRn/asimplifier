package com.imkorn.asimplifier.ribs

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.functions.Consumer

interface Input<In> {
    val input: Consumer<In> get() = Consumer {}
    operator fun component1() = input
}

interface Output<Out> {
    val output: Observable<Out> get() = Observable.empty()
    operator fun component2() = output
}

interface FlowableOutput<Out> {
    val output: Flowable<Out> get() = Flowable.empty()
    operator fun component2() = output
}

interface InputOutput<In, Out> : Input<In>, Output<Out> {
    fun toInputFlowableOutput(
            strategy: BackpressureStrategy
    ): InputFlowableOutput<In, Out> =
            InputOutputToFlowableOutputWrapper(this, strategy)
}

interface InputFlowableOutput<In, Out> : Input<In>, FlowableOutput<Out> {
    fun toInputOutput(): InputOutput<In, Out>
            = InputFlowableOutputToOutputWrapper(this)
}

interface ConsumerInputOutput<In, Out> : InputOutput<In, Out>, Consumer<In> {
    override val input: Consumer<In> get() = this
}

interface ConsumerInputFlowableOutput<In, Out> : InputFlowableOutput<In, Out>, Consumer<In> {
    override val input: Consumer<In> get() = this
}

private class InputOutputToFlowableOutputWrapper<In, Out>(
        source: InputOutput<In, Out>,
        strategy: BackpressureStrategy
) : Input<In> by source, InputFlowableOutput<In, Out> {

    private val flowableOutput = source.output.toFlowable(strategy)

    override val output: Flowable<Out> get() = flowableOutput
}

private class InputFlowableOutputToOutputWrapper<In, Out>(
        source: InputFlowableOutput<In, Out>
) : Input<In> by source, InputOutput<In, Out> {

    private val observableOutput = source.output.toObservable()

    override val output: Observable<Out> get() = observableOutput
}