package com.imkorn.asimplifier.ribs

import androidx.lifecycle.*
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.processors.PublishProcessor
import io.reactivex.subjects.PublishSubject

abstract class IOLiveData<In, Out>(
        initial: In
) : LiveData<In>(), Input<In> {
    init { value = initial }

    override val input: Consumer<In> get() = Consumer { postValue(it) }

    fun observe(owner: LifecycleOwner, input: Input<In>) =
            observe(owner, RxWrappedLifecycleObserver(input.input))

    fun provide(owner: LifecycleOwner, output: Output<Out>) {
        val lifecycle = owner.lifecycle
        val subscriber = createSubscriber(output.output)
        lifecycle.addObserver(RxLifecycleObserver(subscriber))
    }

    fun provide(owner: LifecycleOwner, output: FlowableOutput<Out>) {
        val lifecycle = owner.lifecycle
        val subscriber = createSubscriber(output.output)
        lifecycle.addObserver(RxLifecycleObserver(subscriber))
    }

    fun bind(owner: LifecycleOwner, inputOutput: InputOutput<In, Out>) {
        observe(owner, inputOutput)
        provide(owner, inputOutput)
    }

    fun bind(owner: LifecycleOwner, inputOutput: InputFlowableOutput<In, Out>) {
        observe(owner, inputOutput)
        provide(owner, inputOutput)
    }

    abstract fun createSubscriber(output: Observable<Out>): () -> Disposable
    abstract fun createSubscriber(output: Flowable<Out>): () -> Disposable
}

class InputOutputLiveData<In, Out>(
        initial: In
) : IOLiveData<In, Out>(initial) {

    private val relay = PublishSubject.create<Out>()

    override fun createSubscriber(output: Observable<Out>): () -> Disposable = {
        output.subscribe(relay::onNext)
    }

    override fun createSubscriber(output: Flowable<Out>): () -> Disposable = {
        output.subscribe(relay::onNext)
    }
}

class InputFlowableOutputLiveData<In, Out>(
        initial: In
) : IOLiveData<In, Out>(initial) {

    private val relay = PublishProcessor.create<Out>()

    override fun createSubscriber(output: Observable<Out>): () -> Disposable = {
        output.subscribe(relay::onNext)
    }

    override fun createSubscriber(output: Flowable<Out>): () -> Disposable = {
        output.subscribe(relay::onNext)
    }
}

private class RxLifecycleObserver(
        val subscribe: () -> Disposable 
) : GenericLifecycleObserver {

    private var disposable: Disposable? = null

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        val lifecycle = source.lifecycle
        val state = lifecycle.currentState

        if (state.isAtLeast(Lifecycle.State.STARTED)) {
            disposable = subscribe()
        } else if (state == Lifecycle.State.DESTROYED) {
            lifecycle.removeObserver(this)
            disposable = disposable?.run { dispose(); null }
        }

    }
}

private class RxWrappedLifecycleObserver<Type>(
        val consumer: Consumer<Type>
) : Observer<Type> {
    override fun onChanged(value: Type) {
        consumer.accept(value)
    }
}