package com.imkorn.asimplifier.ribs

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.processors.PublishProcessor
import io.reactivex.subjects.PublishSubject

class ComposedInputOutput<In, Out>(
        private vararg val items: InputOutput<In, Out>
) : InputOutput<In, Out> {

    private val relay = PublishSubject.create<Out>()

    init {
        items.forEach { (_, output) -> output.subscribe(relay) }
    }

    override val input: Consumer<In> get() = Consumer {
        items.forEach { (input, _) -> input.accept(it) }
    }
    override val output: Observable<Out> get() = relay
}

class ComposedInputFlowableOutput<In, Out>(
        private vararg val items: InputFlowableOutput<In, Out>
) : InputFlowableOutput<In, Out> {

    private val relay = PublishProcessor.create<Out>()

    init {
        items.forEach { (_, output) -> output.subscribe(relay) }
    }

    override val input: Consumer<In> get() = Consumer {
        items.forEach { (input, _) -> input.accept(it) }
    }
    override val output: Flowable<Out> get() = relay
}

fun <In, Out> InputOutput<In, Out>.composeWith(
        inputOutput: InputOutput<In, Out>
): InputOutput<In, Out> = ComposedInputOutput(this, inputOutput)

fun <In, Out> InputFlowableOutput<In, Out>.composeWith(
        inputOutput: InputFlowableOutput<In, Out>
): InputFlowableOutput<In, Out> = ComposedInputFlowableOutput(this, inputOutput)