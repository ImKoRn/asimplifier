package com.imkorn.asimplifier.rx2

import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableHelper.DISPOSED
import java.util.concurrent.atomic.AtomicReference

object Disposables {

    val AtomicReference<Disposable>.disposed: Boolean get() = get() === DISPOSED

    fun AtomicReference<Disposable>.disposeAndGetOrNull(): Disposable? {
        var current: Disposable? = get()
        val disposed = DISPOSED
        if (current !== disposed) {
            current = getAndSet(disposed)
            if (current !== disposed) {
                if (current != null) {
                    current.dispose()
                    return current
                }
            }
        }

        return null
    }

}