package com.imkorn.asimplifier.rx2

import com.imkorn.asimplifier.rx2.Subscriptions.canceled
import io.reactivex.Flowable
import io.reactivex.FlowableSubscriber
import io.reactivex.disposables.Disposable
import io.reactivex.internal.subscriptions.SubscriptionHelper.setOnce
import io.reactivex.internal.subscriptions.SubscriptionHelper.cancel
import org.reactivestreams.Subscription
import java.util.concurrent.atomic.AtomicReference

object Flowables {

    fun <T> Flowable<T>.subscribe(
            onSubscribe: (Disposable) -> Unit = {},
            onNext: (T) -> Unit = {},
            onError: (Throwable, Disposable) -> Unit = { t, _ -> throw t },
            onComplete: (Disposable) -> Unit = {}
    ) = subscribe(
            object : FlowableSubscriber<T>, AtomicReference<Subscription>(), Disposable {

                override fun isDisposed(): Boolean = canceled

                override fun dispose() {
                    cancel(this)
                }

                override fun onSubscribe(s: Subscription) {
                    if (setOnce(this, s)) {
                        onSubscribe(this)
                    }
                }

                override fun onComplete() {
                    if (cancel(this)) {
                        onComplete(this)
                    }
                }

                override fun onNext(t: T) {
                    if (!canceled) {
                        onNext(t)
                    }
                }

                override fun onError(e: Throwable) {
                    if (cancel(this)) {
                        onError(e, this)
                    }
                }
            }
    )
}