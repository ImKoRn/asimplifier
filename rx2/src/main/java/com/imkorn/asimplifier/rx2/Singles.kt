package com.imkorn.asimplifier.rx2

import com.imkorn.asimplifier.rx2.Disposables.disposeAndGetOrNull
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableHelper
import java.util.concurrent.atomic.AtomicReference

object Singles {

    fun <T> Single<T>.subscribe(
            onSubscribe: (Disposable) -> Unit = {},
            onSuccess: (T, Disposable) -> Unit = { _, _ -> },
            onError: (Throwable, Disposable) -> Unit = { t, _ -> throw t }
    ) = subscribe(
            object : SingleObserver<T>, AtomicReference<Disposable>() {
                override fun onSuccess(t: T) {
                    onSuccess(t, disposeAndGetOrNull() ?: return)
                }

                override fun onSubscribe(d: Disposable) {
                    if (DisposableHelper.setOnce(this, d)) {
                        onSubscribe(d)
                    }
                }

                override fun onError(e: Throwable) {
                    onError(e, disposeAndGetOrNull() ?: return)
                }
            }
    )

}