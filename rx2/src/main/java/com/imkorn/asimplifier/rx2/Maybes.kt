package com.imkorn.asimplifier.rx2

import com.imkorn.asimplifier.rx2.Disposables.disposeAndGetOrNull
import io.reactivex.Maybe
import io.reactivex.MaybeObserver
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableHelper.setOnce
import java.util.concurrent.atomic.AtomicReference

object Maybes {

    fun <T> Maybe<T>.subscribe(
            onSubscribe: (Disposable) -> Unit = {},
            onSuccess: (T, Disposable) -> Unit = { _, _ -> },
            onError: (Throwable, Disposable) -> Unit = { t, _ -> throw t },
            onComplete: (Disposable) -> Unit = {}
    ) = subscribe(
            object : MaybeObserver<T>, AtomicReference<Disposable>() {

                override fun onComplete() {
                    onComplete(disposeAndGetOrNull() ?: return)
                }

                override fun onSuccess(t: T) {
                    onSuccess(t, disposeAndGetOrNull() ?: return)
                }

                override fun onSubscribe(d: Disposable) {
                    if (setOnce(this, d)) {
                        onSubscribe(d)
                    }
                }

                override fun onError(e: Throwable) {
                    onError(e, disposeAndGetOrNull() ?: return)
                }

            }
    )

}