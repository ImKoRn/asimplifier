package com.imkorn.asimplifier.rx2

import io.reactivex.Observable
import com.imkorn.asimplifier.rx2.Disposables.disposeAndGetOrNull
import com.imkorn.asimplifier.rx2.Disposables.disposed
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableHelper.setOnce
import java.util.concurrent.atomic.AtomicReference

object Observables {

    fun <T> Observable<T>.subscribe(
            onSubscribe: (Disposable) -> Unit = {},
            onNext: (T) -> Unit = {},
            onError: (Throwable, Disposable) -> Unit = { t, _ -> throw t },
            onComplete: (Disposable) -> Unit = {}
    ) = subscribe(
            object : Observer<T>, AtomicReference<Disposable>() {
                override fun onComplete() {
                    onComplete(disposeAndGetOrNull() ?: return)
                }

                override fun onSubscribe(d: Disposable) {
                    if (setOnce(this, d)) {
                        onSubscribe(d)
                    }
                }

                override fun onNext(t: T) {
                    if (!disposed) {
                        onNext(t)
                    }
                }

                override fun onError(e: Throwable) {
                    onError(e, disposeAndGetOrNull() ?: return)
                }
            }
    )

}