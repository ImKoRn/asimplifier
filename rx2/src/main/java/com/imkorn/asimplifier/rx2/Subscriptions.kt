package com.imkorn.asimplifier.rx2

import io.reactivex.internal.subscriptions.SubscriptionHelper.CANCELLED
import org.reactivestreams.Subscription
import java.util.concurrent.atomic.AtomicReference

object Subscriptions {

    val AtomicReference<Subscription>.canceled: Boolean get() = get() === CANCELLED

    fun AtomicReference<Subscription>.cancelAndGetOrNull(): Subscription? {
        var current: Subscription? = get()
        val canceled = CANCELLED
        if (current !== canceled) {
            current = getAndSet(canceled)
            if (current !== canceled) {
                if (current != null) {
                    current.cancel()
                    return current
                }
            }
        }

        return null
    }

}