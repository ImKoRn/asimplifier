package com.imkorn.asimplifier.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.imkorn.asimplifier.core.ContextComponent.Companion.preL
import com.imkorn.asimplifier.ui.UiComponent
import com.imkorn.asimplifier.ui.UiComponent.FinishType

abstract class ActivityBase : AppCompatActivity(), UiComponent<Intent> {

    private lateinit var root: View
    private var valid = true
    
    final override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            valid = savedInstanceState.getBoolean(KEY_VALID)
            onValid { onUiComponentDataRestore(savedInstanceState) }
        } else {
            valid = onUiComponentDataInit(intent?: Intent())
        }

        onValid {
            onUiComponentDataPrepared()
            root = onUiComponentViewCreate().apply { setContentView(this) }
            onUiComponentViewCreated()
            onUiComponentPrepared()
        }
    }

    final override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_VALID, valid)
        onValid { onUiComponentDataSave(outState) }
    }

    final override fun onStart() {
        super.onStart()
        onValid { onUiComponentStart() }
    }

    final override fun onResume() {
        super.onResume()
        onValid { onUiComponentResume() }
    }

    final override fun onPause() {
        super.onPause()
        onValid {
            onUiComponentPause()
        }
    }

    final override fun onStop() {
        super.onStop()
        onValid { onUiComponentStop() }
    }

    final override fun onDestroy() {
        super.onDestroy()
        onValid { onUiComponentDestroy(isFinishing) }
    }

    override fun getRootView(): View = root

    override fun DialogFragment.show(key: String) = show(supportFragmentManager, key)

    override fun Intent.start() = startActivity(this)

    override fun Intent.start(request: Int) = startActivityForResult(this, request)

    override fun getContext(): Context = this

    override fun getViewModelProvider(): ViewModelProvider = ViewModelProviders.of(this)

    override fun finish(type: FinishType, result: Int, payload: Intent?) {
        setResult(result, payload)
        when(type) {
            FinishType.ACTIVITY -> finish()
            FinishType.AFFINITY -> finishAffinity()
            FinishType.TASK     -> if(preL()) finish() else finishAndRemoveTask()
        }
    }
    
    private inline fun <Type> onValid(action: () -> Type): Type? = if (valid) action() else null
}

private const val KEY_VALID = "ASimplifier.FragmentDelegate.KEY_VALID"