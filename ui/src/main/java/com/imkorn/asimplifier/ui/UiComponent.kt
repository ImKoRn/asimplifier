package com.imkorn.asimplifier.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.imkorn.asimplifier.core.actions.CallbackProvider
import com.imkorn.asimplifier.core.ContextComponent
import kotlin.reflect.KClass

interface UiComponent<Initializer> : LifecycleOwner, ContextComponent, CallbackProvider {

    // Lifecycle
    fun onUiComponentDataInit(initializer: Initializer): Boolean = true
    fun onUiComponentDataRestore(bundle: Bundle) {}
    fun onUiComponentDataPrepared() {}
    fun onUiComponentDataSave(bundle: Bundle) {}
    fun onUiComponentViewCreate(container: ViewGroup? = null): View
    fun onUiComponentViewCreated() {}
    fun onUiComponentPrepared() {}
    fun onUiComponentStart() {}
    fun onUiComponentResume() {}
    fun onUiComponentPause() {}
    fun onUiComponentStop() {}
    fun onUiComponentDestroy(finishing: Boolean) {}

    fun finish(
            type: FinishType = FinishType.ACTIVITY,
            result: Int = Activity.RESULT_CANCELED,
            payload: Intent? = null
    )

    // Intent utils
    fun Intent.start()
    fun Intent.start(request: Int)
    fun KClass<*>.asIntent(): Intent = Intent(getContext(), java)
    fun KClass<*>.start() = asIntent().start()
    fun KClass<*>.start(request: Int) = asIntent().start(request)

    // View model utils
    fun getViewModelProvider(): ViewModelProvider
    fun <ViewModelType : ViewModel> getViewModel(clazz: KClass<ViewModelType>, key: String? = null): ViewModelType {
        val provider = getViewModelProvider()
        return if (key != null) {
            provider.get(key, clazz.java)
        } else {
            provider.get(clazz.java)
        }
    }

    // View utils
    fun getRootView(): View
    fun <ViewType : View?> Int.find(): ViewType = getRootView().findViewById<ViewType>(this)

    // Fragment utils
    fun DialogFragment.show(key: String)

    enum class FinishType {
        ACTIVITY,
        AFFINITY,
        TASK
    }
}