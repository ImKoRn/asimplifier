package com.imkorn.asimplifier.ui.fragment

import android.app.Activity
import android.content.Context
import com.imkorn.asimplifier.core.ContextComponent.Companion.preM
import com.imkorn.asimplifier.core.actions.Callback
import kotlin.reflect.KClass

abstract class FragmentBaseAction<ListenerType : Callback>(
        private val callbackClazz: KClass<ListenerType>
) : FragmentBase() {

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)

        if (preM()) callbackRef = findCallback(callbackClazz)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callbackRef = findCallback(callbackClazz)
    }

    override fun onDetach() {
        super.onDetach()
        callbackRef = null
    }

    protected val callback get() = callbackRef?:
    throw NullPointerException("Callback was released or not captured yet")

    private var callbackRef: ListenerType? = null
}
