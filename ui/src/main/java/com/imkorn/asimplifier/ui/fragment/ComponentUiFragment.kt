package com.imkorn.asimplifier.ui.fragment

import android.os.Bundle
import com.imkorn.asimplifier.core.actions.Callback
import com.imkorn.asimplifier.ui.UiComponent
import kotlin.reflect.KClass

interface ComponentUiFragment : UiComponent<Bundle> {
    @Suppress("UNCHECKED_CAST")
    fun <ListenerType : Callback> findCallback(clazz: KClass<ListenerType>): ListenerType
}