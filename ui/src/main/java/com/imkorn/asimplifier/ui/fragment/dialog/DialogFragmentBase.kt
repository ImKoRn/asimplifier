package com.imkorn.asimplifier.ui.fragment.dialog

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.imkorn.asimplifier.core.actions.Callback
import com.imkorn.asimplifier.ui.UiComponent.FinishType
import com.imkorn.asimplifier.ui.fragment.ComponentUiFragment
import com.imkorn.asimplifier.ui.fragment.FragmentDelegate
import kotlin.reflect.KClass

abstract class DialogFragmentBase : AppCompatDialogFragment(), ComponentUiFragment
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        delegate.onCreateDelegate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return delegate.onCreateViewDelegate(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        delegate.onViewCreatedDelegate(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        delegate.onActivityCreatedDelegate(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        delegate.onSaveInstanceStateDelegate(outState)
    }

    override fun onStart() {
        super.onStart()
        delegate.onStartDelegate()
    }

    override fun onResume() {
        super.onResume()
        delegate.onResumeDelegate()
    }

    override fun onPause() {
        super.onPause()
        delegate.onPauseDelegate()
    }

    override fun onStop() {
        super.onStop()
        delegate.onStopDelegate()
    }

    override fun onDestroy() {
        super.onDestroy()
        delegate.onDestroyDelegate()
    }

    override fun getContext(): Context = delegate.getContextDelegate()

    override fun finish(type: FinishType, result: Int, payload: Intent?) {
        delegate.finishDelegate(type, result, payload)
    }

    override fun DialogFragment.show(key: String) {
        delegate.showDelegate(this, key)
    }

    override fun Intent.start() {
        delegate.startDelegate(this)
    }

    override fun Intent.start(request: Int) {
        delegate.startDelegate(this, request)
    }

    override fun getRootView(): View = delegate.getRootViewDelegate()

    override fun getViewModelProvider(): ViewModelProvider = delegate.getViewModelProviderDelegate()

    override fun <ListenerType : Callback> findCallback(clazz: KClass<ListenerType>): ListenerType {
        return delegate.findCallbackDelegate(clazz)
    }

    private val delegate by lazy(LazyThreadSafetyMode.NONE) { FragmentDelegate(this) }
}