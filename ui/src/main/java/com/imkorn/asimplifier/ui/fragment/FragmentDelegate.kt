package com.imkorn.asimplifier.ui.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.imkorn.asimplifier.core.ContextComponent.Companion.preL
import com.imkorn.asimplifier.core.actions.Callback
import com.imkorn.asimplifier.core.actions.CallbackProvider
import com.imkorn.asimplifier.ui.UiComponent.FinishType
import kotlin.reflect.KClass

internal class FragmentDelegate<Component>(
        private val component: Component
) where Component : ComponentUiFragment, Component : Fragment {

    private var valid = true

    fun onCreateDelegate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            valid = savedInstanceState.getBoolean(KEY_VALID)
            onValid { component.onUiComponentDataRestore(savedInstanceState) }
        } else {
            valid = component.onUiComponentDataInit(component.arguments?: Bundle.EMPTY)
        }

        onValid { component.onUiComponentDataPrepared() }
    }

    fun onCreateViewDelegate(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return onValid { component.onUiComponentViewCreate(container) }
    }

    fun onViewCreatedDelegate(view: View, savedInstanceState: Bundle?) {
        onValid { component.onUiComponentViewCreated() }
    }

    fun onActivityCreatedDelegate(savedInstanceState: Bundle?) {
        onValid { component.onUiComponentPrepared() }
    }

    fun onSaveInstanceStateDelegate(outState: Bundle) {
        outState.putBoolean(KEY_VALID, valid)
        onValid { component.onUiComponentDataSave(outState) }
    }

    fun onStartDelegate() {
        onValid { component.onUiComponentStart() }
    }

    fun onResumeDelegate() {
        onValid { component.onUiComponentResume() }
    }

    fun onPauseDelegate() {
        onValid { component.onUiComponentPause() }
    }

    fun onStopDelegate() {
        onValid { component.onUiComponentStop() }
    }

    fun onDestroyDelegate() {
        onValid { component.onUiComponentDestroy(component.activity?.isFinishing ?:false) }
    }

    fun startDelegate(intent: Intent) {
        onValid { component.startActivity(intent) }
    }

    fun startDelegate(intent: Intent, request: Int) {
        onValid { component.startActivityForResult(intent, request) }
    }

    fun getContextDelegate(): Context = component.requireContext()

    fun getViewModelProviderDelegate(): ViewModelProvider = ViewModelProviders.of(component)

    fun getRootViewDelegate(): View = component.view!!

    fun showDelegate(dialogFragment: DialogFragment, key: String) {
        dialogFragment.show(component.fragmentManager, key)
    }

    fun finishDelegate(type: FinishType, result: Int, payload: Intent?) {
        component.activity?.apply {
            setResult(result, payload)

            when(type) {
                FinishType.ACTIVITY -> finish()
                FinishType.AFFINITY -> finishAffinity()
                FinishType.TASK -> if(preL()) finish() else finishAndRemoveTask()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <ListenerCallback : Callback> findCallbackDelegate(clazz: KClass<ListenerCallback>): ListenerCallback {
        val source = component.parentFragment ?: component.activity
        if(source is CallbackProvider) return source.getCallback(clazz) as ListenerCallback
        throw NotImplementedError("$clazz no found in $source")
    }

    private inline fun <Type> onValid(action: () -> Type): Type? = if (valid) action() else null
}

private const val KEY_VALID = "ASimplifier.FragmentDelegate.KEY_VALID"