package com.imkorn.asimplifier.ui.fragment.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.View

import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

abstract class DialogFragmentBaseBottomSheet : DialogFragmentBase() {

    override fun onUiComponentDataRestore(bundle: Bundle) {
        state = bundle.getInt(KEY_SHEET_STATE, BottomSheetBehavior.STATE_EXPANDED)
    }

    override fun onCreateDialog(_savedInstanceState: Bundle?): Dialog {
        val sheetDialog = BottomSheetDialog(context, theme)
        sheetDialog.setOnShowListener { _ ->
            val bottomSheetId = com.google.android.material.R.id.design_bottom_sheet
            val bottomSheet = sheetDialog.findViewById<View>(bottomSheetId) ?: throw IllegalStateException("No bottom sheet found")
            behavior = BottomSheetBehavior.from(bottomSheet).also { behavior ->
                behavior.state = state
                behavior.isFitToContents = isFitToContents
                behavior.skipCollapsed = skipCollapsed
                behavior.isHideable = isHideable
                behavior.peekHeight = peekHeight
                behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                    override fun onStateChanged(bottomSheet: View, newState: Int) {
                        state = newState
                        if (newState == BottomSheetBehavior.STATE_HIDDEN) sheetDialog.cancel()
                        bottomSheetCallback?.onStateChanged(bottomSheet, newState)
                    }

                    override fun onSlide(bottomSheet: View, slideOffset: Float) {
                        bottomSheetCallback?.onSlide(bottomSheet, slideOffset)
                    }
                })
            }
        }
        return sheetDialog
    }

    override fun onUiComponentDataSave(bundle: Bundle) {
        super.onUiComponentDataSave(bundle)
        bundle.putInt(KEY_SHEET_STATE, state)
    }

    var state = BottomSheetBehavior.STATE_EXPANDED
        set(value) {
            behavior.state = value
            field = value
        }
    var isFitToContents = true
        set(value) {
            behavior.isFitToContents = value
            field = value
        }
    var skipCollapsed = false
        set(value) {
            behavior.skipCollapsed = value
            field = value
        }
    var isHideable = false
        set(value) {
            behavior.isHideable = value
            field = value
        }
    var peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
        set(value) {
            behavior.peekHeight = value
            field = value
        }
    var bottomSheetCallback: BottomSheetBehavior.BottomSheetCallback? = null
    private lateinit var behavior: BottomSheetBehavior<View>
}

private const val KEY_SHEET_STATE = "KEY_SHEET_STATE"
