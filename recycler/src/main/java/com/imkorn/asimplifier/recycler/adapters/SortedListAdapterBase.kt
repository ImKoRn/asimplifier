package com.imkorn.asimplifier.recycler.adapters

import android.util.SparseArray
import android.view.LayoutInflater

import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.imkorn.asimplifier.core.collections.Collections.forEach
import com.imkorn.asimplifier.core.collections.Collections.isNotEmpty

class SortedListAdapterBase<ItemType>(
        inflater: LayoutInflater,
        creator: ItemCreator<ItemType>,
        typeProvider: ((position: Int, item: ItemType) -> Int),
        clazz: Class<ItemType>,
        comparator: ItemComparator<ItemType>
) : AdapterBase<ItemType>(inflater, creator, typeProvider) {

    private val list: SortedList<ItemType> = SortedList(clazz, Callback(comparator))

    var batched: Boolean = false; private set

    override fun getCount() = list.size()

    override fun get(position: Int): ItemType = list[position]

    override fun set(position: Int, item: ItemType): ItemType =
            list.get(position).apply { list.updateItemAt(position, item) }

    override fun setAll(items: Array<ItemType>) {
        if(items.isNotEmpty()) {
            list.replaceAll(*items)
        } else {
            clear()
        }
    }

    override fun setAll(items: Collection<ItemType>) {
        if(items.isNotEmpty()) {
            list.replaceAll(items)
        } else {
            clear()
        }
    }

    override fun setAll(items: SparseArray<ItemType>) {
        if(items.isNotEmpty()) {
            if(!batched) list.beginBatchedUpdates() // Force batched update
            forEach { item -> list.add(item) }
            if(!batched) list.endBatchedUpdates() // Finish forced batch update
        } else {
            clear()
        }
    }

    override fun addFirst(item: ItemType) {
        list.add(item)
    }

    override fun addLast(item: ItemType) {
        list.add(item)
    }

    override fun addTo(position: Int, item: ItemType) {
        list.add(item)
    }

    override fun addAll(items: Array<ItemType>) {
        if (items.isNotEmpty()) list.addAll(*items)
    }

    override fun addAll(items: Collection<ItemType>) {
        if (items.isNotEmpty()) list.addAll(items)
    }

    override fun addAll(items: SparseArray<ItemType>) {
        if (items.isNotEmpty()) {
            if(!batched) list.beginBatchedUpdates() // Force batched update
            items.forEach { item -> list.add(item) }
            if(!batched) list.endBatchedUpdates() // Finish forced batch update
        }
    }

    override fun find(item: ItemType): Int = list.indexOf(item)

    override fun forEach(action: (position: Int, item: ItemType) -> Unit) {
        for (position in 0..list.size()) action(position, list.get(position))
    }

    override fun forEachUntil(
            predicate: (position: Int, item: ItemType
    ) -> Boolean): Int {
        for (position in 0..list.size()) {
            if (predicate(position, list[position])) return position
        }
        return super.forEachUntil(predicate)
    }

    override fun <CollectionType> forEachRemove(
            predicate: (Int, ItemType) -> Boolean,
            initializer: () -> CollectionType,
            action: (CollectionType, Int, ItemType) -> Unit
    ): CollectionType {
        val collection = initializer()

        if (list.size() == 0) return collection

        if (!batched) list.beginBatchedUpdates()  // Force batched update
        forEach { position, item ->
            if (predicate(position, item)) {
                list.removeItemAt(position)
                action(collection, position, item)
            }
        }
        if (!batched) list.endBatchedUpdates() // Finish forced batch update
        return collection
    }

    override fun remove(item: ItemType): ItemType? {
        val position = list.indexOf(item)
        if (position < 0) return null
        return removeAt(position)
    }

    override fun removeAt(position: Int): ItemType = list.removeItemAt(position)

    override fun clear() {
        list.clear()
    }

    override fun update(position: Int) {
        if(batched) endBatchedUpdates()
        notifyItemChanged(position)
    }

    override fun update(start: Int, count: Int) {
        if(batched) endBatchedUpdates()
        notifyItemRangeChanged(start, count)
    }

    override fun update(item: ItemType) {
        val position = list.indexOf(item)
        if(position < 0) return
        update(position)
    }

    override fun update(predicate: (Int, ItemType) -> Boolean) {
        if(batched) endBatchedUpdates()
        for(position in 0 until list.size()) {
            if(predicate(position, list[position])) notifyItemChanged(position)
        }
    }

    override fun updateAll() {
        if(batched) endBatchedUpdates()
        notifyDataSetChanged()
    }

    fun beginBatchedUpdates() {
        batched = true
        list.beginBatchedUpdates()
    }

    fun endBatchedUpdates() {
        batched = false
        list.endBatchedUpdates()
    }

    interface ItemComparator<ItemType> : Comparator<ItemType> {
        fun areContentsTheSame(oldItem: ItemType, newItem: ItemType): Boolean
        fun areItemsTheSame(o1: ItemType, o2: ItemType): Boolean
    }

    private inner class Callback(
            val comparator: ItemComparator<ItemType>
    ) : SortedListAdapterCallback<ItemType>(this) {
        override fun compare(o1: ItemType, o2: ItemType): Int = comparator.compare(o1, o2)

        override fun areContentsTheSame(oldItem: ItemType, newItem: ItemType): Boolean =
                comparator.areContentsTheSame(oldItem, newItem)

        override fun areItemsTheSame(o1: ItemType, o2: ItemType): Boolean =
                comparator.areItemsTheSame(o1, o2)
    }
}
