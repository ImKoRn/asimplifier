package com.imkorn.asimplifier.recycler

import android.content.Context
import android.view.View
import android.widget.CompoundButton

import androidx.recyclerview.widget.RecyclerView
import com.imkorn.asimplifier.core.views.ViewHolderBase

open class RecyclerViewHolderBase<ItemType>(
        content: View
) : RecyclerView.ViewHolder(content), ViewHolderBase<ItemType> {

    override fun getContext(): Context = itemView.context

    override fun <Type : View?> Int.find(): Type = itemView.findViewById<Type>(this)

    inline fun withPosition(action: (Int) -> Unit) {
        val position = adapterPosition
        if (position != RecyclerView.NO_POSITION) {
            action(position)
        }
    }

    inline fun withPositionClick(crossinline action: (Int) -> Unit): (View) -> Unit {
        return {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                action(position)
            }
        }
    }

    inline fun withPositionLongClick(crossinline action: (Int) -> Boolean): (View) -> Boolean {
        return {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                action(position)
            } else {
                false
            }
        }
    }

    inline fun withPositionCheck(crossinline action: (Int, Boolean) -> Unit): (CompoundButton, Boolean) -> Unit {
        return { _, checked ->
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                action(position, checked)
            }
        }
    }
}
