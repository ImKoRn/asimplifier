package com.imkorn.asimplifier.recycler.adapters

import android.util.SparseArray
import android.view.LayoutInflater

import androidx.recyclerview.widget.RecyclerView
import com.imkorn.asimplifier.core.collections.Collections.forEach
import java.util.*

open class AdapterBaseList<ItemType>(
        inflater: LayoutInflater,
        creator: ItemCreator<ItemType>,
        typeProvider: (position: Int, item: ItemType) -> Int = { _, _ -> DEFAULT_TYPE },
        private val list: MutableList<ItemType> = ArrayList()
) : AdapterBase<ItemType>(inflater, creator, typeProvider) {

    override fun getCount(): Int = list.size

    override fun get(position: Int): ItemType = list[position]

    override fun set(position: Int, item: ItemType): ItemType =
            list.set(position, item).apply { notifyItemInserted(position) }

    override fun setAll(items: Array<ItemType>) {
        if (items.isEmpty()) {
            clear(true)
            return
        }
        clear(false)
        items.forEach { item -> list.add(item) }
        notifyDataSetChanged()
    }

    override fun setAll(items: Collection<ItemType>) {
        if (items.isEmpty()) {
            clear(true)
            return
        }
        clear(false)
        list.addAll(items)
        notifyDataSetChanged()
    }

    override fun setAll(items: SparseArray<ItemType>) {
        if (items.size() == 0) {
            clear(true)
            return
        }
        clear(false)
        items.forEach { item -> list.add(item) }
        notifyDataSetChanged()
    }

    override fun addFirst(item: ItemType) {
        list.add(0, item)
        notifyItemInserted(0)
    }

    override fun addLast(item: ItemType) {
        list.add(item)
        notifyItemInserted(list.size - 1)
    }

    override fun addTo(position: Int, item: ItemType) {
        list.add(position, item)
        notifyItemInserted(position)
    }

    override fun addAll(items: Array<ItemType>) {
        if (items.isEmpty()) return
        val startSize = list.size
        items.forEach { item -> list.add(item) }
        notifyItemRangeInserted(startSize, list.size - 1)
    }

    override fun addAll(items: Collection<ItemType>) {
        if (items.isEmpty()) return
        val startSize = list.size
        list.addAll(items)
        notifyItemRangeInserted(startSize, list.size - 1)
    }

    override fun addAll(items: SparseArray<ItemType>) {
        if (items.size() == 0) return
        val startSize = list.size
        items.forEach { item -> list.add(item) }
        notifyItemRangeInserted(startSize, list.size - 1)
    }

    override fun find(item: ItemType): Int = list.indexOf(item)

    override fun forEach(action: (Int, ItemType) -> Unit) {
        list.forEachIndexed { position, item -> action(position, item) }
    }

    override fun forEachUntil(predicate: (Int, ItemType) -> Boolean): Int {
        list.forEachIndexed { position, item ->
            if (predicate(position, item)) return position
        }
        return RecyclerView.NO_POSITION
    }

    override fun <CollectionType> forEachRemove(
            predicate: (Int, ItemType) -> Boolean,
            initializer: () -> CollectionType,
            action: (CollectionType, Int, ItemType) -> Unit
    ): CollectionType {
        val collection = initializer()

        if (list.size == 0) return collection

        var position = -1
        list.removeAll { item ->
            position++
            if (predicate(position, item)) {
                action(collection, position, item)
                true
            } else {
                false
            }
        }
        return collection
    }

    override fun remove(item: ItemType): ItemType? {
        val position = list.indexOf(item)
        return if (position < 0) {
            null
        } else {
            removeAt(position)
        }
    }

    override fun removeAt(position: Int): ItemType = list.removeAt(position).apply { notifyItemRemoved(position) }

    override fun clear() = clear(true)

    override fun update(position: Int) = notifyItemChanged(position)

    override fun update(start: Int, count: Int) = notifyItemRangeChanged(start, count)

    override fun update(item: ItemType) {
        val position = find(item)
        if (position < 0) {
            return
        } else {
            update(position)
        }
    }

    override fun update(predicate: (Int, ItemType) -> Boolean) {
        list.forEachIndexed { position, item -> if (predicate(position, item)) update(position) }
    }

    override fun updateAll() = notifyDataSetChanged()

    protected fun clear(notify: Boolean) {
        if (list.isEmpty()) return
        list.clear()
        if (notify) notifyDataSetChanged()
    }
}
