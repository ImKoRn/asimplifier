package com.imkorn.asimplifier.recycler.adapters

import android.view.LayoutInflater
import android.view.ViewGroup

import com.imkorn.asimplifier.core.views.ItemCollection

import androidx.recyclerview.widget.RecyclerView
import com.imkorn.asimplifier.recycler.RecyclerViewHolderBase

typealias ItemCreator<ItemType> = LayoutInflater.(
        type: Int,
        parent: ViewGroup
) -> RecyclerViewHolderBase<out ItemType>

typealias TypeProvider<ItemType> = (position: Int, item: ItemType) -> Int

abstract class AdapterBase<ItemType>(
        private val inflater: LayoutInflater,
        private val creator: ItemCreator<ItemType>,
        private val typeProvider: TypeProvider<ItemType> = { _, _ -> DEFAULT_TYPE })
    : RecyclerView.Adapter<RecyclerViewHolderBase<ItemType>>(), ItemCollection<ItemType> {

    companion object {
        const val DEFAULT_TYPE = 0
    }

    override fun getItemViewType(position: Int): Int = typeProvider(position, get(position))

    override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
    ): RecyclerViewHolderBase<ItemType> {
        @Suppress("UNCHECKED_CAST")
        return creator(inflater, viewType, parent) as RecyclerViewHolderBase<ItemType>
    }

    override fun onBindViewHolder(
            holder: RecyclerViewHolderBase<ItemType>,
            position: Int
    ) = holder.bind(get(position))

    override fun getItemCount(): Int = getCount()
}
