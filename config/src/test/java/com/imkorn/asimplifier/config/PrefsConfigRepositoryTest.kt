package com.imkorn.asimplifier.config

import android.content.SharedPreferences
import com.nhaarman.mockitokotlin2.mock
import org.junit.Test
import com.imkorn.asimplifier.config.ConfigRepository.Companion.readable
import com.imkorn.asimplifier.config.ConfigRepository.Companion.writable
import com.imkorn.asimplifier.config.ConfigRepository.Companion.bool
import com.imkorn.asimplifier.config.ConfigRepository.Companion.int
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn

private val READABLE = "READABLE".readable(bool(true))
private val WRITABLE = "WRITABLE".writable(int(5))
private val EXTERNAL = "EXTERNAL".readable(bool(true))

class PrefsConfigRepositoryTest {

    @Test
    fun `contains in repository`() {
        val subject = create(mock {
            on { registerOnSharedPreferenceChangeListener(any()) } doAnswer {}
        })
        subject.contains(EXTERNAL).test().assertValue(false)
    }

    @Test
    fun `first read from repository`() {
        val subject = create(mock {
            on { registerOnSharedPreferenceChangeListener(any()) } doAnswer {}
            on { getBoolean(any(), any()) } doReturn true
        })
        subject.get(READABLE).test().assertValue(true)
    }

    @Test
    fun `put into repository`() {
        var value = WRITABLE.descriptor.default

        val editor = mock<SharedPreferences.Editor> {
            on { putInt(any(), any()) } doAnswer {
                value = it.getArgument(1)
                it.mock as SharedPreferences.Editor
            }
            on { apply() } doAnswer {}
        }

        val subject = create(mock {
            on { registerOnSharedPreferenceChangeListener(any()) } doAnswer {}
            on { getInt(any(), any()) } doAnswer { value }
            on { edit() } doReturn editor
        })

        subject.put(WRITABLE, 3).test().assertComplete()
        subject.get(WRITABLE).test().assertValue(3)
    }

    @Test
    fun `receive update from repository`() {
        var value = WRITABLE.descriptor.default
        var listener: SharedPreferences.OnSharedPreferenceChangeListener? = null

        val prefs = mock<SharedPreferences> {
            on { getInt(any(), any()) } doAnswer { value }
        }
        val editor = mock<SharedPreferences.Editor> {
            on { putInt(any(), any()) } doAnswer {
                value = it.getArgument(1)
                listener?.onSharedPreferenceChanged(prefs, it.getArgument(0))
                it.mock as SharedPreferences.Editor
            }
            on { apply() } doAnswer {}
        }

        val subject = create(mock {
            on { registerOnSharedPreferenceChangeListener(any()) } doAnswer {
                listener = it.getArgument(0)
                null
            }
            on { getInt(any(), any()) } doAnswer { value }
            on { edit() } doReturn editor
        })

        val test = subject.get(WRITABLE).test()

        subject.put(WRITABLE, 3).test().assertComplete()

        test.assertValues(5, 3)
    }

    private fun create(prefs: SharedPreferences) = PrefsConfigRepository(prefs)
}