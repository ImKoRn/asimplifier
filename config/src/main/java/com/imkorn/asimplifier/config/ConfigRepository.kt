package com.imkorn.asimplifier.config

import io.reactivex.Observable
import io.reactivex.Completable
import io.reactivex.Single

class ConfigCollisionException(config: Config<*, *>) : IllegalStateException(config.toString())

interface ConfigRepository {

    companion object {

        fun <Type : Any, ReadableDescriptor> String.readable(
                descriptor: ReadableDescriptor
        ): ReadableConfig<Type, ReadableDescriptor> where ReadableDescriptor : Descriptor<Type>,
                                                          ReadableDescriptor : Readable {
            return ReadableConfig(this, descriptor)
        }
        fun <Type : Any, WritableDescriptor> String.writable(
                descriptor: WritableDescriptor
        ): WritableConfig<Type, WritableDescriptor> where WritableDescriptor : Descriptor<Type>,
                                                          WritableDescriptor : Readable,
                                                          WritableDescriptor : Writable {
            return WritableConfig(this, descriptor)
        }

        fun bool(default: Boolean) = BooleanDescriptor(default)
        fun int(default: Int) = IntDescriptor(default)
        fun long(default: Long) = LongDescriptor(default)
        fun float(default: Float) = FloatDescriptor(default)
        fun double(default: Double) = DoubleDescriptor(default)
        fun string(default: String) = StringDescriptor(default)
        fun stringSet(default: Set<String>) = StringSetDescriptor(default)
        fun <Type : Any> custom(
                default: Type,
                reader: (String?) -> Type?,
                writer: (Type) -> String? = { null }
        ) = WritableCustomDescriptor(default, reader, writer)
        fun <Type : Any> custom(
                default: Type,
                reader: (String?) -> Type?
        ) = ReadableCustomDescriptor(default, reader)
        fun <Type : Any> const(default: Type) = ConstDescriptor(default)
    }

    fun <Type : Any, WritableConfig, WritableDescriptor> put(
            config: WritableConfig,
            value: Type
    ): Completable where WritableDescriptor : Descriptor<Type>,
                         WritableDescriptor : Writable,
                         WritableConfig : Config<Type, WritableDescriptor>,
                         WritableConfig : Writable

    fun <Type : Any, ReadableConfig, ReadableDescriptor> get(
            config: ReadableConfig
    ): Observable<Type> where ReadableDescriptor : Descriptor<Type>,
                              ReadableDescriptor : Readable,
                              ReadableConfig : Config<Type, ReadableDescriptor>,
                              ReadableConfig : Readable

    fun contains(config: Config<*, *>): Single<Boolean>

    fun <Type: Any, WritableConfig, WritableDescriptor> clean(
            config: WritableConfig
    ): Completable where WritableDescriptor : Descriptor<Type>,
                         WritableDescriptor : Writable,
                         WritableConfig : Config<Type, WritableDescriptor>,
                         WritableConfig : Writable

    fun cleanAll(): Completable
}

interface Readable
interface Writable

sealed class Config<Type : Any, DescriptorType : Descriptor<Type>> {

    abstract val tag: String
    abstract val descriptor: DescriptorType

    val key: String by lazy(LazyThreadSafetyMode.NONE) { "$tag:${descriptor.tag}" }
}

data class ReadableConfig<Type : Any, ReadableDescriptor>(
        override val tag: String,
        override val descriptor: ReadableDescriptor
) : Config<Type, ReadableDescriptor>(), Readable
        where ReadableDescriptor : Descriptor<Type>,
              ReadableDescriptor : Readable

data class WritableConfig<Type : Any, WritableDescriptor>(
        override val tag: String,
        override val descriptor: WritableDescriptor
) : Config<Type, WritableDescriptor>(), Readable, Writable
        where WritableDescriptor : Descriptor<Type>,
              WritableDescriptor : Readable,
              WritableDescriptor : Writable

sealed class Descriptor<Type : Any>(val tag: String) {
    abstract val default: Type
}

data class ConstDescriptor<Type : Any>(
        override val default: Type
) : Descriptor<Type>(TAG), Readable {
    companion object {
        const val TAG = "CONST"
    }
}

data class BooleanDescriptor(
        override val default: Boolean
) : Descriptor<Boolean>(TAG), Readable, Writable {
    companion object {
        const val TAG = "BOOLEAN"
    }
}

data class IntDescriptor(
        override val default: Int
) : Descriptor<Int>(TAG), Readable, Writable {
    companion object {
        const val TAG = "INT"
    }
}

data class LongDescriptor(
        override val default: Long
) : Descriptor<Long>(TAG), Readable, Writable {
    companion object {
        const val TAG = "LONG"
    }
}

data class FloatDescriptor(
        override val default: Float
) : Descriptor<Float>(TAG), Readable, Writable {
    companion object {
        const val TAG = "FLOAT"
    }
}

data class DoubleDescriptor(
        override val default: Double
) : Descriptor<Double>(TAG), Readable, Writable {
    companion object {
        const val TAG = "DOUBLE"
    }
}

data class StringDescriptor(
        override val default: String
) : Descriptor<String>(TAG), Readable, Writable {
    companion object {
        const val TAG = "STRING"
    }
}

data class StringSetDescriptor(
        override val default: Set<String>
) : Descriptor<Set<String>>(TAG), Readable, Writable {
    companion object {
        const val TAG = "STRING_SET"
    }
}

sealed class CustomDescriptor<Type : Any> : Descriptor<Type>(TAG), Readable {
    companion object {
        const val TAG = "CUSTOM"
    }

    abstract val reader: (String?) -> Type?
}
data class ReadableCustomDescriptor<Type : Any>(
        override val default: Type,
        override val reader: (String?) -> Type?
) : CustomDescriptor<Type>()
data class WritableCustomDescriptor<Type : Any>(
        override val default: Type,
        override val reader: (String?) -> Type?,
        val writer: (Type) -> String?
) : CustomDescriptor<Type>(), Writable
