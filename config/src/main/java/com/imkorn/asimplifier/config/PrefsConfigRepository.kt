package com.imkorn.asimplifier.config

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import io.reactivex.Completable
import io.reactivex.Completable.fromRunnable
import io.reactivex.Observable
import io.reactivex.Observable.error
import io.reactivex.Single
import io.reactivex.Single.fromCallable
import io.reactivex.subjects.BehaviorSubject.createDefault
import io.reactivex.subjects.Subject
import java.util.concurrent.ConcurrentHashMap

class PrefsConfigRepository(
        private val prefs: SharedPreferences
) : ConfigRepository {

    private val listener = object : OnSharedPreferenceChangeListener {
        override fun onSharedPreferenceChanged(prefs: SharedPreferences, key: String) {
            val (config, subject) = streams[key]?:return
            subject.onNext(prefs.get(config))
        }
    }

    private val streams = ConcurrentHashMap<String, Pair<Config<*, *>, Subject<Any>>>()

    init {
        prefs.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun <Type : Any, ReadableConfig, ReadableDescriptor> get(
            config: ReadableConfig
    ): Observable<Type> where ReadableConfig : Config<Type, ReadableDescriptor>,
                              ReadableConfig : Readable,
                              ReadableDescriptor : Descriptor<Type>,
                              ReadableDescriptor : Readable {
        val (saved, subject) = streams.getOrElse(config.key) {
            val info = config to createDefault(prefs.get(config))
            streams.putIfAbsent(config.key, info)?: info
        }

        return if (saved == config) {
            @Suppress("UNCHECKED_CAST")
            subject.hide() as Observable<Type>
        } else {
            error(ConfigCollisionException(config))
        }
    }

    override fun <Type : Any, WritableConfig, WritableDescriptor> put(
            config: WritableConfig, value: Type
    ): Completable where WritableDescriptor : Descriptor<Type>,
                         WritableDescriptor : Writable,
                         WritableConfig : Writable,
                         WritableConfig : Config<Type, WritableDescriptor> {
        val key = config.key
        val descriptor = config.descriptor
        return fromRunnable {
            transaction {
                @Suppress("UNCHECKED_CAST")
                when (descriptor) {
                    is BooleanDescriptor -> putBoolean(key, value as Boolean)
                    is IntDescriptor -> putInt(key, value as Int)
                    is LongDescriptor -> putLong(key, value as Long)
                    is FloatDescriptor -> putFloat(key, value as Float)
                    is DoubleDescriptor -> putString(key, value.toString())
                    is StringDescriptor -> putString(key, value as String)
                    is StringSetDescriptor -> putStringSet(key, value as Set<String>)
                    is WritableCustomDescriptor<*> -> {
                        descriptor as WritableCustomDescriptor<Type>
                        putString(key, descriptor.writer(value))
                    }
                }
            }
        }
    }

    override fun contains(config: Config<*, *>): Single<Boolean> {
        return fromCallable { streams.contains(config) || prefs.contains(config.key) }
    }

    override fun <Type : Any, WritableConfig, WritableDescriptor> clean(
            config: WritableConfig
    ): Completable where WritableDescriptor : Descriptor<Type>,
                         WritableDescriptor : Writable,
                         WritableConfig : Config<Type, WritableDescriptor>,
                         WritableConfig : Writable {
        return fromRunnable { transaction { remove(config.key) } }
    }

    override fun cleanAll(): Completable {
        return fromRunnable { transaction { clear() } }
    }

    protected fun finalize() {
        prefs.unregisterOnSharedPreferenceChangeListener(listener)
    }

    private inline fun transaction(block: SharedPreferences.Editor.() -> Unit) {
        with(prefs.edit()) {
            block()
            apply()
        }
    }

    private fun SharedPreferences.get(config: Config<*, *>): Any {
        val key = config.key
        val descriptor = config.descriptor
        return when (descriptor) {
            is BooleanDescriptor -> getBoolean(key, descriptor.default)
            is IntDescriptor -> getInt(key, descriptor.default)
            is LongDescriptor -> getLong(key, descriptor.default)
            is FloatDescriptor -> getFloat(key, descriptor.default)
            is DoubleDescriptor -> getString(key, null)?.toDoubleOrNull()?:descriptor.default
            is StringDescriptor -> getString(key, descriptor.default)
            is StringSetDescriptor -> getStringSet(key, descriptor.default)
            is CustomDescriptor -> descriptor.reader(getString(key, null))?:descriptor.default
            is ConstDescriptor -> descriptor.default
        }
    }
}