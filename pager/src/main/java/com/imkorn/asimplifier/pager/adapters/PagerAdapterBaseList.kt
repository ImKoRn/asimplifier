package com.imkorn.asimplifier.pager.adapters

import android.util.SparseArray
import androidx.recyclerview.widget.RecyclerView
import com.imkorn.asimplifier.core.collections.Collections.forEach
import java.util.ArrayList

abstract class PagerAdapterBaseList<ItemType>
constructor(private val list: MutableList<ItemType> = ArrayList()) : PagerAdapterBase<ItemType>() {

    override fun getCount(): Int = list.size

    override fun get(position: Int): ItemType = list[position]

    override fun set(position: Int, item: ItemType): ItemType = list.set(position, item).apply { notifyDataSetChanged() }

    override fun setAll(items: Array<ItemType>) {
        if (items.isEmpty()) {
            clear(true)
            return
        }
        clear(false)
        items.forEach { item -> list.add(item) }
        notifyDataSetChanged()
    }

    override fun setAll(items: Collection<ItemType>) {
        if (items.isEmpty()) {
            clear(true)
            return
        }
        clear(false)
        list.addAll(items)
        notifyDataSetChanged()
    }

    override fun setAll(items: SparseArray<ItemType>) {
        if (items.size() == 0) {
            clear(true)
            return
        }
        clear(false)
        items.forEach { item -> list.add(item) }
        notifyDataSetChanged()
    }

    override fun addFirst(item: ItemType) {
        list.add(0, item)
        notifyDataSetChanged()
    }

    override fun addLast(item: ItemType) {
        list.add(item)
        notifyDataSetChanged()
    }

    override fun addTo(position: Int, item: ItemType) {
        list.add(position, item)
        notifyDataSetChanged()
    }

    override fun addAll(items: Array<ItemType>) {
        if (items.isEmpty()) return
        items.forEach { item -> list.add(item) }
        notifyDataSetChanged()
    }

    override fun addAll(items: Collection<ItemType>) {
        if (items.isEmpty()) return
        list.addAll(items)
        notifyDataSetChanged()
    }

    override fun addAll(items: SparseArray<ItemType>) {
        if (items.size() == 0) return
        items.forEach { item -> list.add(item) }
        notifyDataSetChanged()
    }

    override fun find(item: ItemType): Int = list.indexOf(item)

    override fun forEach(action: (Int, ItemType) -> Unit) {
        list.forEachIndexed { position, item -> action(position, item) }
    }

    override fun forEachUntil(predicate: (Int, ItemType) -> Boolean): Int {
        list.forEachIndexed { position, item -> if (predicate(position, item)) return position }
        return RecyclerView.NO_POSITION
    }

    override fun <CollectionType> forEachRemove(
            predicate: (position: Int, item: ItemType) -> Boolean,
            initializer: () -> CollectionType,
            action: (collection: CollectionType, position: Int, item: ItemType) -> Unit
    ): CollectionType {
        val collection = initializer()

        if (list.size == 0) return collection

        var position = -1
        list.removeAll { item ->
            position++
            if (predicate(position, item)) {
                action(collection, position, item)
                true
            } else {
                false
            }
        }
        return collection
    }

    override fun remove(item: ItemType): ItemType? {
        val position = list.indexOf(item)
        if (position < 0) return null
        return removeAt(position)
    }

    override fun removeAt(position: Int): ItemType = list.removeAt(position).apply { notifyDataSetChanged() }

    override fun clear() {
        clear(true)
    }

    override fun update(position: Int) {
        notifyDataSetChanged()
    }

    override fun update(start: Int, count: Int) {
        notifyDataSetChanged()
    }

    override fun update(item: ItemType) {
        val position = find(item)
        if (position < 0) return
        else update(position)
    }

    override fun update(predicate: (Int, ItemType) -> Boolean) {
        list.forEachIndexed { position, item -> if (predicate(position, item)) update(position) }
    }

    override fun updateAll() {
        notifyDataSetChanged()
    }

    protected fun clear(notify: Boolean) {
        if (list.isEmpty()) return
        list.clear()
        if (notify) notifyDataSetChanged()
    }
}
