package com.imkorn.asimplifier.pager.adapters

import android.content.Context
import android.view.View
import com.imkorn.asimplifier.core.views.ViewHolderBase

open class ViewHolderBasePager<ItemType>(val itemView: View) : ViewHolderBase<ItemType> {
    override fun <Type : View?> Int.find(): Type = itemView.findViewById<Type>(this)

    override fun getContext(): Context = itemView.context

    var adapterPosition = PagerAdapterBaseListView.NO_POSITION
        internal set
    internal var type = PagerAdapterBaseListView.TYPE_DEFAULT
}