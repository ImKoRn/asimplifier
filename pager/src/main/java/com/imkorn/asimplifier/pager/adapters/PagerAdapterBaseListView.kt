package com.imkorn.asimplifier.pager.adapters

import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import java.util.*
import com.imkorn.asimplifier.core.collections.Collections.isNullOrEmpty
import com.imkorn.asimplifier.core.collections.Collections.getValueOrElse

open class PagerAdapterBaseListView<ItemType>(
        private val creator: (type: Int, parent: ViewGroup) -> ViewHolderBasePager<out ItemType>,
        private val typeProvider: ((position: Int, item: ItemType) -> Int)? = null,
        list: MutableList<ItemType> = ArrayList()) : PagerAdapterBaseList<ItemType>(list) {

    companion object {
        const val TYPE_DEFAULT = 0
        const val NO_POSITION = 0
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean =
            `object` is ViewHolderBasePager<*> && view == `object`.itemView

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        get(position).let { item ->
            val type = typeProvider?.invoke(position, item) ?: TYPE_DEFAULT

            val holder: ViewHolderBasePager<ItemType>

            val list = pools[type]
            @Suppress("UNCHECKED_CAST")
            holder = if(list.isNullOrEmpty()) {
                creator(type, container).apply { this.type = type }
            } else {
                list.removeFirst()
            } as ViewHolderBasePager<ItemType>

            holder.adapterPosition = position
            container.addView(holder.itemView)
            holder.bind(item)

            return holder
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, holder: Any) {
        if(holder is ViewHolderBasePager<*>) {
            holder.adapterPosition = NO_POSITION
            container.removeView(holder.itemView)

            if(keepHolder(holder)) {
                val pool = pools.getValueOrElse(holder.type) {
                    val list = LinkedList<ViewHolderBasePager<*>>()
                    put(holder.type, list)
                    list
                }
                pool.addLast(holder)
            }
        }
    }

    open fun keepHolder(holder: ViewHolderBasePager<*>) = true

    private val pools = SparseArray<LinkedList<ViewHolderBasePager<*>>>()
}