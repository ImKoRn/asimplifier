package com.imkorn.asimplifier.pager.adapters

import androidx.viewpager.widget.PagerAdapter
import com.imkorn.asimplifier.core.views.ItemCollection

abstract class PagerAdapterBase<ItemType> : PagerAdapter(), ItemCollection<ItemType>
